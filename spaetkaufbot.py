import json
import urllib
from urllib.parse import quote_plus
import config
import requests
import time
import logging

from dbhelper import DBHelper
from stateHandler import STATEHandler
from geohelper import SpaetiGeoHelper

db = DBHelper()
states = STATEHandler()
spaetiGeo = SpaetiGeoHelper()

TOKEN = config.token
URL = "https://api.telegram.org/bot{}/".format(TOKEN)

logging.basicConfig(level=logging.INFO,
                    filename="Spaeti_log.log",
                    format="%(asctime)s:%(levelname)s:%(message)s")


def get_url(url):
    response = requests.get(url)
    content = response.content.decode("utf8")
    return content


def get_json_from_url(url):
    content = get_url(url)
    js = json.loads(content)
    return js


def get_updates(offset=None):
    url = URL + "getUpdates"
    if offset:
        # Identifier of the first update to be returned
        url += "?offset={}".format(offset)
    js = get_json_from_url(url)
    return js


def get_last_update_id(updates):
    update_ids = []
    for update in updates["result"]:
        update_ids.append(int(update["update_id"]))
    return max(update_ids)


def message_dic(state):
    """
    :param state: current state
    :return: the corresponding message
    """
    switch = {
        "location": "Your Location: Either transmit your GPS position or enter a street name manually",
        "spaeti_id": "Is the Späti's name one of those?",
        "price": "What's the price (in Eurocents)?"
    }
    return switch.get(state, "Invalid input")


#############
user_db = {}  # temporary database, before setting it permanently with DBHelper


def initiate_user_tmp_db():
    return {key: None for key in states.get_states()}


def reset_userdata(chat_id):
    states.reset_userstate(chat_id)
    user_db[chat_id] = initiate_user_tmp_db()


def get_userdata(chat_id):
    """
    Returns the entry which hasn't fully be saved yet.

    :param chat_id: The ID to identificate the current user
    :return: The entry from the not-yet persistent database
    """
    if chat_id not in user_db:
        reset_userdata(chat_id)
    return user_db[chat_id]


def set_userdata_and_increase_state(chat_id, state, text):
    user_db[chat_id][state] = text


def overview(items):
    """
    Function to print item tuples pretty.

    :param items:
    :return:
    """
    allitems = [str(x[1]) + " with price " + str(x[2]) + " on " + str(x[3]) for x in items]
    allitems.insert(0, "Currently from you in Database:")
    return "\n".join(allitems)


####################


def handle_updates(updates):
    for update in updates["result"]:
        if "message" not in update:
            logging.info("Something other than a message has been recorded: ", update)
            continue
        chat_id = update["message"]["chat"]["id"]

        if not db.isin_user_db(chat_id):
            chat = update["message"]["chat"]
            if "first_name" in chat:
                first_name = chat["first_name"]
            else:
                first_name = "NA"
            if "last_name" in chat:
                last_name = chat["last_name"]
            else:
                last_name = "NA"
            db.add_user(chat_id=chat_id, first_name=first_name, last_name=last_name)
        state = states.get_userstate(chat_id)
        get_userdata(chat_id)  # to initiate
        items = [str(x) for x in db.get_item(chat_id)]
        if "text" in update["message"]:
            text = update["message"]["text"]
            print(text)
            if text in ("/start", "/new"):
                send_message("You can /start or use /delete to delete an entry", chat_id)
                reset_userdata(chat_id)
                keyboard = location_keyboard()
                message = message_dic(states.get_userstate(chat_id))
                send_message(message, chat_id, keyboard)
            elif text == "/delete":
                # TODO Add 'None' to delete nothing and abort the deleting process
                items.insert(0, "skip")
                keyboard = build_keyboard(items)
                send_message("Select an entry to delete", chat_id, keyboard)
            elif text in items:
                # Delete if in items
                logging.info("Delete entry: ", text)
                text = eval(text)
                db.delete_item(text, text[-1])
                send_message(f"Deleted: {text}", chat_id)
            elif state == "price" and text != "skip":
                try:
                    string_int = int(text)  # if fails, catch error
                    if string_int < 0 or string_int > 400:
                        raise ValueError("negative price")
                    set_userdata_and_increase_state(chat_id, states.get_userstate(chat_id), string_int)
                    states.increase_userstate(chat_id)
                except ValueError as v:
                    # Ask for another try
                    mesg = "The price you entered was invalid or too high. Enter a price in Eurocents, e.g. '50' if " \
                           "the Sterni was 50 Eurocents.\nError message is :" + str(v)
                    send_message(mesg, chat_id)
                    continue
            elif state == "spaeti_id" and text != "skip":
                spaeti_name = text.split(" ---- ", 1)
                spaeti_id = spaeti_name[0]
                if spaeti_id == "None":
                    spaeti_id = "NA"
                set_userdata_and_increase_state(chat_id, state, spaeti_id)
                states.increase_userstate(chat_id)
                send_message(message_dic(states.get_userstate(chat_id)), chat_id)
            elif state == "location" and text != "skip":
                # In case user has entered an address instead of GPS location
                set_userdata_and_increase_state(chat_id, states.get_userstate(chat_id), text)
                states.increase_userstate(chat_id)
                states.increase_userstate(chat_id)  # Skip Späti name
                send_message(message_dic(states.get_userstate(chat_id)), chat_id)
            elif text == "skip":
                continue
            else:
                # show all elements if any other text was added
                items = db.get_item(chat_id)
                message = overview(items)
                send_message(message, chat_id)
        elif "location" in update["message"]:
            if state == "location":
                location = spaetiGeo.wkt(update["message"]["location"])
                set_userdata_and_increase_state(chat_id, state, location)
                states.increase_userstate(chat_id)
                # next state: name of Späti
                nearest_spaeties = spaetiGeo.nearest_spaetis(location, 200)
                nearest_spaeties = nearest_spaeties.head(n=5)
                k = ["None"]
                if not nearest_spaeties.empty:
                    k_spaetis = nearest_spaeties.apply(
                        lambda temp: "%s ---- %s meter" % (str(temp['name']), str(round(temp['distances'], 2))),
                        axis=1).tolist()
                    [k.append(c_i) for c_i in k_spaetis]
                keyboard = build_keyboard(k)
                msg = message_dic(states.get_userstate(chat_id))
                send_message(msg, chat_id, keyboard)
            else:
                # When user is at the wrong stage
                send_message("Currently you are expected to enter " + states.get_userstate(chat_id) + ". You can "
                                                                                                      "restart with "
                                                                                                      "/new.", chat_id)
                send_message(message_dic(states.get_userstate(chat_id)), chat_id)
        if states.get_userstate(chat_id) == "done":
            user_tuple = get_userdata(chat_id)
            db.add_item(user_tuple=user_tuple, owner=chat_id)
            location = user_tuple["location"]
            name = user_tuple["spaeti_id"]
            price = user_tuple["price"]
            msg = f"added position {location} ({name}) with price {price} ct. Restart..."
            send_message(msg, chat_id)
            reset_userdata(chat_id)
            # Start from the beginning
            keyboard = location_keyboard()
            msg = message_dic(states.get_userstate(chat_id))
            send_message(msg, chat_id, keyboard)


def get_last_chat_id_and_text(updates):
    num_updates = len(updates["result"])
    last_update = num_updates - 1
    text = updates["result"][last_update]["message"]["text"]  # get the actual text
    chat_id = updates["result"][last_update]["message"]["chat"]["id"]
    return text, chat_id


def build_keyboard(items):
    keyboard = [[item] for item in items]
    reply_markup = {"keyboard": keyboard, "one_time_keyboard": True}
    return json.dumps(reply_markup)


def location_keyboard():
    keyboard = [[{"text": "Share location", "request_location": True}]]
    reply_markup = {"keyboard": keyboard, "one_time_keyboard": True}
    return json.dumps(reply_markup)


def send_message(text, chat_id, reply_markup=None):
    text = quote_plus(text)
    url = URL + "sendMessage?text={}&chat_id={}&parse_mode=Markdown".format(text, chat_id)
    if reply_markup:
        reply_markup = urllib.parse.quote(reply_markup, safe="")
        url += "&reply_markup={}".format(reply_markup)
    get_url(url)


def main():
    db.setup()
    last_update_id = None
    while True:
        updates = get_updates(last_update_id)
        if len(updates["result"]) > 0:
            last_update_id = get_last_update_id(updates) + 1
            handle_updates(updates)
        time.sleep(0.5)


if __name__ == '__main__':
    main()
