import sqlite3


class DBHelper:

    def __init__(self, dbname="spaeti.sqlite"):
        self.dbname = dbname
        self.conn = sqlite3.connect(dbname)

    def setup(self):
        tblstmt = "CREATE TABLE IF NOT EXISTS items (id integer primary key, location text, price real, date text, " \
                  "spaeti_id integer,  owner text) "
        itemidx = "CREATE INDEX IF NOT EXISTS itemIndex ON items (location ASC)"
        ownidx = "CREATE INDEX IF NOT EXISTS ownIndex ON items (owner ASC)"
        self.conn.execute(tblstmt)
        self.conn.execute(itemidx)
        self.conn.execute(ownidx)
        user_db = "CREATE TABLE IF NOT EXISTS user (chat_id integer primary key, first_name text, last_name)"
        self.conn.execute(user_db)
        self.conn.commit()

    def add_item(self, user_tuple, owner):
        print("added position: " + user_tuple["location"] + " with price: " + str(user_tuple["price"]))
        # TODO geht das auch geiler?
        stmt = "INSERT INTO items (location, price, date, owner) VALUES (?, ?, date('now'), ?)"  # UTC
        args = (user_tuple["location"], user_tuple["price"], owner)
        self.conn.execute(stmt, args)
        self.conn.commit()

    def delete_item(self, text, owner):
        print(text)
        stmt = "DELETE FROM items WHERE id = (?) AND owner = (?)"
        args = (str(text[0]), owner)
        self.conn.execute(stmt, args)
        self.conn.commit()
        print("Deleted position: " + str(text[0]) + " with price: " + str(text[2]) + " cents of owner " + owner)

    def get_item(self, owner):
        stmt = "SELECT * FROM items WHERE owner = (?)"
        args = (owner,)
        # return [x[0] for x in self.conn.execute(stmt, args)]
        cur = self.conn.execute(stmt, args)
        return tuple(cur.fetchall())

    def get_user(self):
        stmt = "SELECT chat_id FROM user"
        cur = self.conn.execute(stmt)
        return cur.fetchall()

    def add_user(self, chat_id, first_name, last_name):
        stmt = "INSERT INTO user (chat_id, first_name, last_name) VALUES (?,?,?)"
        args = (chat_id, first_name, last_name)
        self.conn.execute(stmt, args)
        self.conn.commit()

    def isin_user_db(self, chat_id):
        all_user = self.get_user()
        # all_user = all_user
        all_user = list([a[0] for a in all_user])
        return chat_id in all_user
