class STATEHandler:

    def __init__(self):
        self._states = ["location", "spaeti_id", "price"]
        self._states.append("done")
        self.user_states = {}

    def get_states(self):
        states_tmp = self._states.copy()
        states_tmp.remove("done")
        return states_tmp

    def get_userstate(self, chat_id):
        if chat_id not in self.user_states:
            self.reset_userstate(chat_id)
        return self.user_states[chat_id]

    def reset_userstate(self, chat_id):
        self.user_states[chat_id] = self._states[0]

    def increase_userstate(self, chat_id):
        # get index of current state
        i = self._states.index(self.get_userstate(chat_id))
        # Falls letzter Eintrag, setze auf 0 zurück
        if i == (len(self._states) - 1):
            self.reset_userstate(chat_id)
        else:
            # increase state
            self.user_states[chat_id] = self._states[i + 1]
