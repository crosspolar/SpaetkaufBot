import geojson
import overpass
from shapely import wkt
from shapely.ops import transform
from shapely.geometry import Point
from jsondiff import diff
import geopandas as gdp
import pyproj
from os.path import exists


class SpaetiGeoHelper:

    def __init__(self):
        self._geojson_path = 'geo_spaeti.geojson'
        with open(self._geojson_path, 'r') as f:
            self._spaetis = geojson.load(f)

    def nearest_spaetis(self, pos, perimeter=200):
        """
        Calculates distances to Spätis within distance

        :param pos: point position of observer as text (soon also as shapely.Point)
        :param perimeter: perimeter to search [m]
        :return: Spätis within perimeter, sorted by distance
        """
        # print("geohelper Input: " + pos)
        pos = wkt.loads(str(pos))
        pos_33N_ETRS89 = self.point_project(pos)
        pos_buf = pos_33N_ETRS89.buffer(perimeter)
        with open(self._geojson_path, 'r') as f:
            result = geojson.load(f)
        spaetis = gdp.GeoDataFrame.from_features(result).drop_duplicates()
        spaetis = spaetis.set_crs('epsg:4326')
        spaetis_33N_ETRS89 = spaetis.to_crs('epsg:25833')  # ETRS89 / UTM zone 33N
        spaeties_within_pos_buf = spaetis_33N_ETRS89[spaetis_33N_ETRS89.within(pos_buf)]
        spaeties_within_pos_buf._is_copy = False
        spaeties_within_pos_buf['distances'] = spaeties_within_pos_buf.distance(pos_33N_ETRS89)
        return spaeties_within_pos_buf.sort_values(by='distances')

    @staticmethod
    def point_project(coord):
        """
        Changes projection of a Point in EPSG:4326 to EPSG:25833 to be able to calculate in meter

        :param coord: shapely Point feature in EPSG:4326
        :return:
        """
        wgs84 = pyproj.CRS('EPSG:4326')
        utm = pyproj.CRS('EPSG:25833')
        project = pyproj.Transformer.from_crs(wgs84, utm, always_xy=True).transform
        transformed_coord = transform(project, coord)
        return transformed_coord

    def update_spaetifile(self):
        api = overpass.API()
        result = api.get("""
        area["ISO3166-2"="DE-BE"]->.a;
        (
          node(area.a)["shop"="convenience"];
          node(area.a)["shop"="kiosk"];

        );
        out center;
        """, responseformat="geojson")

        if exists(self._geojson_path):
            with open(self._geojson_path, 'r') as f:
                old_geojson = geojson.load(f)
                # check for differences and if difference between old and new JSON --> save
                test = diff(result, old_geojson)
        else:
            # file does not exist yet
            test = True

        if bool(test):
            print("File does not eixst or there is a difference between old and new json")
            with open('geo_spaeti.geojson', 'w') as f:
                geojson.dump(result, f)

    @staticmethod
    def wkt(location):
        """
        :param location: location as returned by Telegram Update JSON
        :return: point as WKT
        """
        return Point(location['longitude'], location['latitude']).wkt
